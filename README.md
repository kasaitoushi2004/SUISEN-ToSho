# SUISEN-ToSho

笠井闘志個人ウェブサイト内の「おすすめ本リスト」（ https://kasaitoushi.nagano.jp/libroj ）に使用している，PHPスクリプト。

見やすさや修正のしやすさのため，複数のechoコマンドに分けています。これをまとめても正常に動作します。