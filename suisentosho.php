<?php

function suisentosho($isbn,$syomei,$tyosya,$syuppansya,$honyakusya,$shoukai){
echo '<h3 class="Book"><dc:title>'.$syomei.'</dc:title></h3>';
echo '<ul><li><dc:author>'.$tyosya.'</dc:author>氏著</li> ';

if ($honyakusya != null) {
echo '<li><dc:translater>'.$honyakusya.'</dc:translater>氏訳</li> ';
}

echo '<li><dc:publisher>'.$syuppansya.'</dc:publisher></li>';
echo '<li><a href="urn:isbn:'.$isbn.'"><abbr lang="en" title="International Standard Book Number">ISBN</abbr>：<dc:identifier>'.$isbn.'</dc:identifier></a></li>';
echo '<li><a href="https://iss.ndl.go.jp/books?rft.isbn='.$isbn.'">国立国会図書館</a></li>';
echo '<li><a href="https://calil.jp/book/'.$isbn.'">カーリル（全国図書館検索）</a></p></li>';
echo '</ul><dc:description>'.$shoukai.'</dc:description>';
}

?>